# DISCLAIMER
This app was created, rather "committed", relatively long time ago. While some of the project is written properly and can be of use for someone, it is full of bad practices and now it serves no other purpose rather than show that I am actually making progress ;).
Please **DO NOT** follow the deign patterns used in this project

# Phone Link

First version of "my vision" of an app connecting Android powered device with ~~Windows~~(Windows 10)  powered device. 

## Project status

 - [x] Dead
 - [x] Total rewrite in progress

## Installation

Please don't.

## Build With

* [IntelliJ IDEA](https://www.jetbrains.com/idea/)

## Authors

Paweł Wojtczak

## License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details

## Note

The idea to create such an app was inspired by [KDE Connect](https://community.kde.org/KDEConnect) - thanks for them for inspiring my "main project".

