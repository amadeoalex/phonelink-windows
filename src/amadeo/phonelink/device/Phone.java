package amadeo.phonelink.device;

import amadeo.phonelink.Log;
import org.json.JSONObject;

import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.net.*;
import java.util.Base64;

//TODO: change this to freaking JSON...

public class Phone {
    private static final String TAG = "Phone";

    private final String deviceID;
    private final byte[] certificate;

    private InetAddress lastKnownPCAddress;
    private int tcpPort;

    private int listeningTCPPort;

    public Phone(String deviceID, byte[] certificate, InetAddress inetAddress, int tcpPort, int listeningTCPPort) {
        this.deviceID = deviceID;
        this.certificate = certificate;

        this.lastKnownPCAddress = inetAddress;
        this.tcpPort = tcpPort;
        this.listeningTCPPort = listeningTCPPort;
    }

    public static Phone deserialize(String serializedJSON) {
        JSONObject jsonObject = new JSONObject(serializedJSON);

        byte[] secret = Base64.getDecoder().decode(jsonObject.getString("certificate"));
        InetAddress inetAddress = null;
        try {
            inetAddress = InetAddress.getByName(jsonObject.getString("inetAddress"));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }


        return new Phone(jsonObject.getString("deviceID"), secret, inetAddress, jsonObject.getInt("tcpPort"), jsonObject.getInt("listeningTCPPort"));
    }


    public String getDeviceID() {
        return deviceID;
    }

    public byte[] getEncodedCertificate() {
        return certificate;
    }

    public InetAddress getLastKnownPCAddress() {
        return lastKnownPCAddress;
    }

    public int getTcpPort() {
        return tcpPort;
    }

    public int getListeningTCPPort() {
        return listeningTCPPort;
    }

    @Override
    public String toString() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("deviceID", deviceID);
        jsonObject.put("certificate", new String(Base64.getEncoder().encode(certificate)));
        jsonObject.put("inetAddress", lastKnownPCAddress.getHostAddress());
        jsonObject.put("tcpPort", tcpPort);
        jsonObject.put("listeningTCPPort", listeningTCPPort);

        return jsonObject.toString();
    }
}
