package amadeo.phonelink.features.smsnotifications;

import org.json.JSONObject;

import java.util.HashMap;

public class SingleMessage extends HashMap<String, String> {
    private static final long serialVersionUID = 1L;

    public SingleMessage(String name, String data, String date) {
        put("name", name);
        put("body", data);
        put("date", date);
    }

    public SingleMessage(JSONObject object) {
        put("name", object.optString("name", null));
        put("body", object.getString("body"));
        put("date", object.getString("date"));
    }
}
