package amadeo.phonelink.features.smsnotifications;

import amadeo.phonelink.Log;
import amadeo.phonelink.PhoneLinkUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class Conversation extends JSONObject {

    private static final String TAG = "Conversation";

    public Conversation(String number) {
        put("number", number);
        put("preferences", new JSONObject(PhoneLinkUtils.loadResourceAsString("resources/json/default_conversation_settings.json")));
        put("conversation", new JSONArray());
    }

    public Conversation(JSONObject object) {
        put("number", object.getString("number"));
        put("name", object.optString("name", getString("number")));
        put("preferences", object.getJSONObject("preferences"));
        put("conversation", object.getJSONArray("conversation"));
    }

    public String getNumber() {
        return getString("number");
    }

    public String getName() {
        return optString("name", getString("number"));
    }

    public void setName(String name) {
        put("name", name);
    }

    private JSONArray getConversationArray() {
        return getJSONArray("conversation");
    }

    public void addMessage(JSONObject singleMessage) {
        getConversationArray().put(singleMessage);
    }

    public JSONArray messages() {
        return getJSONArray("conversation");
    }

    private JSONObject getLastMessageJSON() {
        if (getConversationArray().length() == 0) {
            Log.i(TAG, "conv size==0");

            return new JSONObject().put("body", "").put("date", "");
        }

        return (JSONObject) getConversationArray().get(getConversationArray().length() - 1);
    }

    public JSONObject getLastMessage() {
        return getLastMessageJSON();
    }

    public void updateTimeStamp(String date, String body) {
        JSONObject lastMessage = getLastMessageJSON();
        String currentLastDate = lastMessage.getString("date");
        String currentLastBody = lastMessage.getString("body");

        if (currentLastBody.equals(body) && currentLastDate.equals(date)) {
            Log.i(TAG, "same body and date");
        } else {
            if (currentLastBody.equals(body)) {
                lastMessage.put("date", date);
                Log.i(TAG, "updating date from/to: %s/%s |BODY COMPARISON %s/%s", currentLastDate, date, currentLastBody, body);
            } else {
                Log.i(TAG, "message not updated, different body (C:%s /= %s", currentLastBody, body);
            }
        }
    }

    public void removeLastMessage() {
        getConversationArray().remove(getConversationArray().length() - 1);
    }
}
