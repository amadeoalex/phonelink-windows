package amadeo.phonelink.features.smsnotifications.ui.conversation;

import amadeo.phonelink.features.smsnotifications.SingleMessage;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;


public class MessageBubble extends HBox {
    private final Color SENT_COLOR = Color.rgb(139, 193, 255);
    private final Color RECEIVED_COLOR = Color.rgb(166, 255, 175);
    private final Color INVALIDATED_COLOR = Color.rgb(255, 100, 100);

    private final int curve = 8;

    private SingleMessage message;
    private Label messageLabel;


    public MessageBubble(SingleMessage message) {
        this.message = message;

        messageLabel = new Label();
        messageLabel.setWrapText(true);
        messageLabel.setPadding(new Insets(4));
        messageLabel.setText(message.get("body"));
        if (message.get("name") == null) {
            messageLabel.setAlignment(Pos.CENTER_RIGHT);
            messageLabel.setBackground(new Background(new BackgroundFill(SENT_COLOR, new CornerRadii(curve, curve, curve, curve, false), Insets.EMPTY)));
            setAlignment(Pos.CENTER_RIGHT);
            setPadding(new Insets(0, 4, 0, 40));
        } else {
            messageLabel.setAlignment(Pos.CENTER_LEFT);
            messageLabel.setBackground(new Background(new BackgroundFill(RECEIVED_COLOR, new CornerRadii(curve, curve, curve, curve, false), Insets.EMPTY)));
            setAlignment(Pos.CENTER_LEFT);
            setPadding(new Insets(0, 40, 0, 4));
        }

        HBox bubbleContainer = new HBox(messageLabel);
        getChildren().setAll(new BorderPane(bubbleContainer));
    }

    public SingleMessage getMessage() {
        return message;
    }

    public void invalidate() {
        messageLabel.setBackground(new Background(new BackgroundFill(INVALIDATED_COLOR, new CornerRadii(curve, curve, curve, curve, false), Insets.EMPTY)));
    }
}
