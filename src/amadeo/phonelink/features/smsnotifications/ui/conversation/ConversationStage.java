package amadeo.phonelink.features.smsnotifications.ui.conversation;

import amadeo.phonelink.DataProvider;
import amadeo.phonelink.Log;
import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.features.smsnotifications.Conversation;
import amadeo.phonelink.features.smsnotifications.SMS;
import amadeo.phonelink.features.smsnotifications.SingleMessage;
import amadeo.phonelink.network.manager.ConnectionManager;
import amadeo.phonelink.network.NetPackage;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.json.JSONObject;

public class ConversationStage extends Stage {
    //TODO: unify naming scheme - ConversationStage & SMSNotification?

    private final ConversationVBox conversationVBox;

    public ConversationStage(String number) {
        Conversation conversation = DataProvider.getConversation(number);

        setTitle(conversation.getName());
        getIcons().setAll(PhoneLinkUtils.icon);

        conversationVBox = new ConversationVBox(conversation);
        Scene scene = new Scene(conversationVBox);
        setScene(scene);

        setOnShowing(event -> conversationVBox.onShowing());
    }

    public void notify(NetPackage netPackage) {
        conversationVBox.notify(netPackage);
    }

    private class ConversationVBox extends VBox {
        private final VBox messagesVBox;
        private final ScrollPane messagesScrollPane;
        private final HBox responseHBox;
        private final TextField response;
        private final ObservableList<Node> messageBubbles = FXCollections.observableArrayList();

        private Conversation conversation;

        public ConversationVBox(Conversation conversation) {
            super(4);
            setId("scene");

            this.conversation = conversation;

            messagesVBox = new VBox(4);
            messagesScrollPane = new ScrollPane(messagesVBox);
            messagesScrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
            messagesScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
            messagesScrollPane.setPrefHeight(300);
            messagesScrollPane.prefWidthProperty().bind(messagesVBox.prefWidthProperty().subtract(4));
            messagesScrollPane.setFitToWidth(true);
            messagesScrollPane.setFitToHeight(false);
            Bindings.bindContentBidirectional(messageBubbles, messagesVBox.getChildren());

            messagesVBox.heightProperty().addListener((observable, oldValue, newValue) -> {
                messagesVBox.layout();
                messagesScrollPane.setVvalue(1.0d);
            });

            responseHBox = new HBox(4);
            response = new TextField();
            response.setPromptText("Enter SMS");
            response.setPrefWidth(200);
            HBox.setHgrow(response, Priority.ALWAYS);
            response.setOnKeyPressed(event -> {
                if (event.getCode() == KeyCode.ENTER)
                    sendResponse();
            });
            Button sendButton = new Button("Send");
            sendButton.setOnAction(event -> sendResponse());
            responseHBox.getChildren().setAll(response, sendButton);

            getChildren().setAll(messagesScrollPane, responseHBox);
            setPadding(new Insets(4));
            minHeight(getPrefHeight());
            minWidth(getPrefWidth());
        }

        public void onShowing(){
            for (Object o : conversation.messages()) {
                SingleMessage singleMessage = new SingleMessage((JSONObject) o);
                messageBubbles.add(new MessageBubble(singleMessage));
            }

            String body = conversation.getLastMessage().getString("body");
            sendRead(body);
        }

        private void sendResponse() {
            String message = response.getText();
            response.setText("");

            if (!message.isEmpty()) {
                String timeStamp = Long.toString(System.currentTimeMillis());
                DataProvider.addToConversation(conversation.getNumber(), null, message, timeStamp);
                messageBubbles.add(new MessageBubble(new SingleMessage(null, message, timeStamp)));

                NetPackage netPackage = new NetPackage("sms_send_sms");
                netPackage.setString("number", conversation.getNumber());
                netPackage.setString("body", message);
                netPackage.setString("date", Long.toString(System.currentTimeMillis()));
                ConnectionManager.getConnectionServer().sendNetPackage(netPackage);
            }
        }

        private void sendRead(String body) {
            NetPackage netPackage = new NetPackage("sms_read_sms");
            netPackage.setString("body", body);
            ConnectionManager.getConnectionServer().sendNetPackage(netPackage);
        }

        public void notify(NetPackage netPackage) {
            String type = netPackage.type().replaceFirst("sms_", "");
            SMS sms = new SMS(netPackage);

            switch (type) {
                case "sms":
                    addMessageBubble(sms);
                    sendRead(sms.body);
                    break;
                case "send_denied":
                    invalidateMessageBubble(sms);
                    break;
                case "sent_sms":
                    JSONObject lastMessage = DataProvider.getConversation(sms.number).getLastMessage();
                    Log.i("ConversationVBox", "Message: %s, LBody: %s, Body: %s, LDate: %s, Date: %s", lastMessage, lastMessage.get("body"), sms.body, lastMessage.get("date"), sms.date);
                    sms.name = null;
                    addMessageBubble(sms);
                    break;
            }
        }

        public void addMessageBubble(SMS sms) {
            messageBubbles.add(new MessageBubble(new SingleMessage(sms.name, sms.body, sms.date)));
        }

        private void invalidateMessageBubble(SMS sms) {
            for (int i = messageBubbles.size() - 1; i >= 0; i--) {
                MessageBubble messageBubble = (MessageBubble) messageBubbles.get(i);
                SingleMessage message = messageBubble.getMessage();
                if (message.get("body").equals(sms.body)) {
                    messageBubble.invalidate();
                    break;
                }
            }
        }

        public void reloadConversation(Conversation conversation) {
            this.conversation = conversation;

            messageBubbles.clear();
            for (Object o : conversation.messages()) {
                SingleMessage singleMessage = new SingleMessage((JSONObject) o);
                messageBubbles.add(new MessageBubble(singleMessage));
            }
        }
    }
}
