package amadeo.phonelink.features.smsnotifications.ui.notification;

import amadeo.phonelink.features.smsnotifications.SMS;
import amadeo.phonelink.features.smsnotifications.ui.conversation.ConversationStage;
import amadeo.phonelink.ui.WindowManagerTest;
import amadeo.phonelink.ui.swing.AnimManager;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import javax.swing.*;

public class SMSNotification extends JDialog {

    public SMSNotification(SMS sms) {
        setLocationRelativeTo(null);
        setUndecorated(true);
        setName(sms.number);

        JFXPanel rootPanel = new JFXPanel();

        Platform.runLater(() -> {
            Scene scene = new Scene(new SMSNotificationVBox(sms));
            scene.addEventFilter(MouseEvent.MOUSE_RELEASED, event -> {
                WindowManagerTest.getInstance().showFXStage(sms.number, new ConversationStage(sms.number));
                dispose();
            });
            rootPanel.setScene(scene);

            SwingUtilities.invokeLater(() -> {
                getContentPane().add(rootPanel);

                pack();
                setAlwaysOnTop(true);

                Rectangle2D scrBounds = WindowManagerTest.getInstance().getScreen().getVisualBounds();

                int yPos = (int) (scrBounds.getMinY() + scrBounds.getHeight() - getHeight());
                int xTo = (int) (scrBounds.getMinX() + scrBounds.getWidth() - getWidth());

                setLocation(xTo, yPos);
                setVisible(true);
                AnimManager.animAlpha(this, 0, 1);

                new Thread(() -> {
                    try {
                        Thread.sleep(5500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    dispose();
                }).start();
            });
        });
    }

    private class SMSNotificationVBox extends VBox {
        public SMSNotificationVBox(SMS sms) {
            setPrefSize(256, 128);
            setBorder(new Border(new BorderStroke(Color.CORNFLOWERBLUE, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(0, 2, 2, 6))));

            HBox titleHBox = new HBox(2);
            titleHBox.setBackground(new Background(new BackgroundFill(Color.CORNFLOWERBLUE, CornerRadii.EMPTY, Insets.EMPTY)));

            Label numberLabel = new Label(sms.name);
            numberLabel.setMaxWidth(Double.MAX_VALUE);
            numberLabel.setAlignment(Pos.CENTER);
            numberLabel.setPadding(new Insets(4));
            numberLabel.setTextFill(Color.WHITE);
            HBox.setHgrow(numberLabel, Priority.ALWAYS);

            Separator titleSeparator = new Separator(Orientation.VERTICAL);
            titleSeparator.setPadding(new Insets(2, 0, 2, 0));
            titleSeparator.setPrefWidth(1);

            Label dateLabel = new Label(sms.getTimeString());
            dateLabel.setAlignment(Pos.CENTER);
            dateLabel.setPadding(new Insets(4));
            dateLabel.setTextFill(Color.WHITE);

            titleHBox.getChildren().setAll(numberLabel, titleSeparator, dateLabel);

            Label bodyLabel = new Label(sms.body);
            bodyLabel.setWrapText(true);
            bodyLabel.setPadding(new Insets(0, 2, 0, 2));


            getChildren().setAll(titleHBox, bodyLabel);
        }
    }
}
