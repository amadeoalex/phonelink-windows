package amadeo.phonelink.features.smsnotifications;

import amadeo.phonelink.DataProvider;
import amadeo.phonelink.Log;
import amadeo.phonelink.features.Feature;
import amadeo.phonelink.features.FeatureManager;
import amadeo.phonelink.features.smsnotifications.ui.conversation.ConversationStage;
import amadeo.phonelink.features.smsnotifications.ui.notification.SMSNotification;
import amadeo.phonelink.network.manager.ConnectionManager;
import amadeo.phonelink.network.NetPackage;
import amadeo.phonelink.ui.WindowManagerTest;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.swing.*;

public class SMSNotificationsFeature extends Feature {
    private static final String TAG = "SMSNotificationsFeature";
    private JSONObject preferences;

    private final String SYNC_ON_CONNECT = "syncOnConnect";
    private final String SYNC_NAMES_ON_CONNECT = "syncNamesOnConnect";

    public SMSNotificationsFeature() {
        super("sms_", TAG);

        preferences = getPreferences();
    }

    @Override
    public void onCreate() {
        Log.i(TAG, "%s created.", TAG);
    }

    @Override
    public void onStart() {
        Log.i(TAG, "%s started.", TAG);
    }

    @Override
    public void onConnected() {
        if (DataProvider.getContacts().size() == 0)
            return;

        if (preferences.optBoolean(SYNC_NAMES_ON_CONNECT, true)) {
            NetPackage netPackage = new NetPackage("sms_conversation_name_from_number");

            JSONArray numbers = new JSONArray();
            for (String number : DataProvider.getContacts()) {
                String name = DataProvider.getConversation(number).getName();
                if (name == null || name.equals(number)) {
                    numbers.put(number);
                }
            }
            netPackage.setJSONArray("numbers", numbers);

            if (numbers.length() > 0)
                ConnectionManager.getConnectionServer().sendPriorityNetPackage(netPackage);
        }

        if (preferences.optBoolean(SYNC_ON_CONNECT, true)) {
            NetPackage netPackage = new NetPackage("sms_conversation_sync");
            JSONArray numbers = new JSONArray();
            JSONObject lastMessages = new JSONObject();

            for (String number : DataProvider.getContacts()) {
                numbers.put(number);
                lastMessages.put(number, DataProvider.getConversation(number).getLastMessage());
            }

            netPackage.setJSONArray("numbers", numbers);
            netPackage.setJSONObject("last_messages", lastMessages);

            if (numbers.length() > 0)
                ConnectionManager.getConnectionServer().sendPriorityNetPackage(netPackage);
        }
    }

    @Override
    public void onNetPackageReceived(NetPackage netPackage) {
        String type = netPackage.type().replaceFirst(getPrefix(), "");

        switch (type) {
            case "sms": {
                SMS sms = new SMS(netPackage);

                String currentName = DataProvider.getConversation(sms.number).getName();
                if (!sms.name.equals(sms.number) && !currentName.equals(sms.name)) {
                    DataProvider.getConversation(sms.number).setName(sms.name);
                    Log.i(TAG, "conversation %s name updated from %s to %s", sms.number, currentName, sms.name);
                }


                DataProvider.addToConversation(sms);

                SwingUtilities.invokeLater(() -> {
                    WindowManagerTest.getInstance().showAWTWindow(sms.number, new SMSNotification(sms));
                    ConversationStage conversationStage = (ConversationStage) WindowManagerTest.getInstance().getFXStage(sms.number);
                    if (conversationStage != null)
                        Platform.runLater(() -> conversationStage.notify(netPackage));
                });
                break;
            }
            case "send_denied": {
                invalidateMessageBubble(netPackage);
                break;
            }

            case "sent_sms": {
                Log.w(TAG, "SENT SMS");
                //TODO: cleanup this mess i.e creating SMS 1235236 times...
                SMS sms = new SMS(netPackage);

                Conversation conversation = DataProvider.getConversation(sms.number);
                JSONObject lastMessage = conversation.getLastMessage();
                if (!lastMessage.get("body").equals(sms.body) || !lastMessage.get("date").equals(sms.date)) {
                    sms.name = null;
                    DataProvider.addToConversation(sms);

                    Platform.runLater(() -> {
                        ConversationStage conversationStage = (ConversationStage) WindowManagerTest.getInstance().getFXStage(sms.number);
                        if (conversationStage != null)
                            conversationStage.notify(netPackage);
                    });
                }
                break;
            }


            case "conversation_sync_data":
                JSONObject messages = netPackage.getJSONObject("messages");

                for (Object o : netPackage.getJSONArray("numbers")) {
                    String number = (String) o;

                    JSONArray singleConversationMessages = messages.getJSONArray(number);
                    DataProvider.addToConversation(number, singleConversationMessages);

                    System.out.printf("%s: (%d): ", number, singleConversationMessages.length());
                    for (Object o2 : singleConversationMessages) {
                        JSONObject sm = (JSONObject) o2;
                        System.out.print(sm.get("body") + ", ");
                    }
                    System.out.println();
                }
                break;

            case "conversation_name_from_number":
                JSONObject numberNames = netPackage.getJSONObject("number_names");
                for (Object o : numberNames.keySet()) {
                    String number = (String) o;

                    DataProvider.getConversation(number).setName(numberNames.getString(number));
                }

                break;

            case "update_time_stamp":
                String number = netPackage.getString("number");
                DataProvider.getConversation(number).updateTimeStamp(netPackage.getString("date"), netPackage.getString("body"));
                break;

            default:
                Log.i(TAG, "SMS Agent: unknown package -%s- received", netPackage.type());
                break;
        }
    }

    @Override
    public void onNetPackageFailed(NetPackage netPackage) {
        netPackage.setType("send_denied");
        invalidateMessageBubble(netPackage);
    }

    @Override
    public Tab getSettingsTab() {
        Tab tab = new Tab(getName());

        VBox mainVBox = new VBox(4);
        mainVBox.setPadding(new Insets(4));

        VBox settingsVBox = new VBox(4);

        CheckBox enabledButton = new CheckBox("Enabled");
        enabledButton.setOnAction(event -> {
            if (enabledButton.isSelected()) {
                Log.i(TAG, "enabling");
                FeatureManager.enableFeature(getName());
            } else {
                Log.i(TAG, "disabling");
                FeatureManager.disableFeature(getName());
            }
        });
        enabledButton.setSelected(FeatureManager.isEnabled(getName()));


        CheckBox syncOnConnectButton = new CheckBox("Synchronize messages each time phone connects with pc.");
        syncOnConnectButton.setSelected(preferences.optBoolean(SYNC_ON_CONNECT, true));
        syncOnConnectButton.setOnAction(event -> {
            if (syncOnConnectButton.isSelected()) {
                preferences.put(SYNC_ON_CONNECT, true);
            } else {
                preferences.put(SYNC_ON_CONNECT, false);
            }
        });

        CheckBox syncNamesOnConnectButton = new CheckBox("Synchronize contacts names each time phone connects with pc.");
        syncNamesOnConnectButton.setSelected(preferences.optBoolean(SYNC_NAMES_ON_CONNECT, true));
        syncNamesOnConnectButton.setOnAction(event -> {
            if (syncNamesOnConnectButton.isSelected()) {
                preferences.put(SYNC_NAMES_ON_CONNECT, true);
            } else {
                preferences.put(SYNC_NAMES_ON_CONNECT, false);
            }
        });

        settingsVBox.getChildren().setAll(new Separator(), syncOnConnectButton, syncNamesOnConnectButton);
        settingsVBox.disableProperty().bind(Bindings.not(enabledButton.selectedProperty()));
        mainVBox.getChildren().setAll(enabledButton, settingsVBox);

        tab.setContent(mainVBox);

        return tab;
    }

    @Override
    public void onStop() {
        Log.i(TAG, "SMS Agent stopped.");
    }

    private void invalidateMessageBubble(NetPackage netPackage) {
        SMS sms = new SMS(netPackage);
        SwingUtilities.invokeLater(() -> {
            ConversationStage conversationStage = (ConversationStage) WindowManagerTest.getInstance().getFXStage(sms.name);
            conversationStage.notify(netPackage);
        });

        Conversation conversation = DataProvider.getConversation(sms.number);
        JSONObject lastMessage = conversation.getLastMessage();
        if (lastMessage.get("body").equals(sms.body) && lastMessage.get("date").equals(sms.date)) {
            DataProvider.getConversation(sms.number).removeLastMessage();
        }
    }
}
