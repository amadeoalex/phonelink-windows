package amadeo.phonelink.features.smsnotifications;

import amadeo.phonelink.network.NetPackage;

import java.text.SimpleDateFormat;

public class SMS {
    public String date;
    public String number;
    public String body;
    public String name;

    public SMS(NetPackage netPackage) {
        this.date = netPackage.getString("date");
        this.number = netPackage.getString("number");
        this.body = netPackage.getString("body");
        this.name = netPackage.getString("name");
        if (this.name == null || this.name.equals(""))
            this.name = number;
    }

    public SMS(String date, String number, String body, String name) {
        this.date = date;
        this.number = number;
        this.body = body;
        if (name == null)
            this.name = number;
        else
            this.name = name;
    }

    public String getTimeString() {
        return new SimpleDateFormat("HH:mm").format(Long.parseLong(date));
    }
}