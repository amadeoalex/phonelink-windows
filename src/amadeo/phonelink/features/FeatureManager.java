package amadeo.phonelink.features;

import amadeo.phonelink.Log;
import amadeo.phonelink.PreferenceManager;
import amadeo.phonelink.features.linkshare.LinkShareFeature;
import amadeo.phonelink.features.smsnotifications.SMSNotificationsFeature;
import amadeo.phonelink.network.NetPackage;
import org.json.JSONObject;

import java.util.concurrent.ConcurrentHashMap;

public class FeatureManager {
    private static final String TAG = "FeatureManager";
    private static ConcurrentHashMap<String, Feature> features = new ConcurrentHashMap<>();
    private static JSONObject disabledFeatures = PreferenceManager.getPreferences("disabledFeatures");

    public static void init() {
        registerFeature(new SMSNotificationsFeature());
        registerFeature(new LinkShareFeature());
    }

    public static void registerFeature(Feature feature) {
        features.put(feature.getName(), feature);
        feature.onCreate();

        if (disabledFeatures.optString(feature.getName(), null) == null) {
            feature.onStart();
        } else {
            Log.i(TAG, "feature %s registered but disabled", feature.getName());
        }

    }

    public static void enableFeature(String name) {
        disabledFeatures.remove(name);

        Feature feature = getFeature(name);
        feature.onStart();
    }

    public static void disableFeature(String name) {
        disabledFeatures.put(name, "");

        Feature feature = getFeature(name);
        feature.onStop();
    }

    public static boolean isEnabled(String name) {
        return !disabledFeatures.has(name);
    }

    //TODO: UNSAFE - change to return list of names to be used with getFeature()
    public static ConcurrentHashMap<String, Feature> getFeatures() {
        return features;
    }

    public static Feature getFeature(String name) {
        return features.get(name);
    }

    public static void onConnected() {
        features.forEach((s, feature) -> {
            if (isEnabled(feature.getName()))
                feature.onConnected();
        });
    }

    public static synchronized void parseNetPackage(NetPackage netPackage) {
        Log.i(TAG, "parsing packet '%s'", netPackage.type());

        features.forEach((s, feature) -> {
            if (isEnabled(feature.getName()) && netPackage.type().startsWith(feature.getPrefix()))
                feature.onNetPackageReceived(netPackage);
        });
    }

    public static synchronized void failedNetPackage(NetPackage netPackage) {
        features.forEach((s, feature) -> {
            if (isEnabled(feature.getName()) && netPackage.type().startsWith(feature.getPrefix()))
                feature.onNetPackageFailed(netPackage);
        });
    }

    public static void cleanup() {
        features.forEach((s, feature) -> {
            if (isEnabled(feature.getName()))
                feature.onStop();
        });
    }
}
