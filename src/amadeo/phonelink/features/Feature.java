package amadeo.phonelink.features;

import amadeo.phonelink.PreferenceManager;
import amadeo.phonelink.network.NetPackage;
import javafx.scene.control.Tab;
import org.json.JSONObject;

public abstract class Feature {
    private final String prefix;
    private final String name;

    public Feature(String prefix, String name) {
        this.prefix = prefix;
        this.name = name;
    }

    public void onCreate() {
    }


    public void onStart() {
    }


    public void onStop() {
    }


    public void onConnected() {
    }


    public void onNetPackageReceived(NetPackage netPackage) {
    }

    public void onNetPackageFailed(NetPackage netPackage){
    }


    public String getPrefix() {
        return prefix;
    }

    public String getName() {
        return name;
    }

    public Tab getSettingsTab() {
        return null;
    }

    public JSONObject getPreferences() {
        JSONObject featureSettings = PreferenceManager.getPreferences("featureSettings");
        if (!featureSettings.has(getName()))
            featureSettings.put(getName(), new JSONObject());

        return featureSettings.getJSONObject(getName());
    }

    public String getFilesPath() {
        return "features/" + getName() + "/";
    }
}
