package amadeo.phonelink.features.linkshare.ui;

import amadeo.phonelink.ui.WindowManagerTest;
import amadeo.phonelink.ui.swing.AnimManager;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public class LinkNotification extends JDialog {
    public LinkNotification(String date, String url) {
        setLocationRelativeTo(null);
        setUndecorated(true);

        JFXPanel rootPanel = new JFXPanel();

        Platform.runLater(() -> {
            Scene scene = new Scene(new LinkNotificationVBox(date, url));
            scene.addEventFilter(MouseEvent.MOUSE_RELEASED, event -> {
                try {
                    URI uri = new URL(url).toURI();
                    Desktop.getDesktop().browse(uri);
                } catch (IOException | URISyntaxException e) {
                    e.printStackTrace();
                }
                dispose();
            });
            rootPanel.setScene(scene);

            SwingUtilities.invokeLater(() -> {
                getContentPane().add(rootPanel);

                pack();
                setAlwaysOnTop(true);

                Rectangle2D scrBounds = WindowManagerTest.getInstance().getScreen().getVisualBounds();

                int yPos = (int) (scrBounds.getMinY() + scrBounds.getHeight() - getHeight());
                int xTo = (int) (scrBounds.getMinX() + scrBounds.getWidth() - getWidth());

                setLocation(xTo, yPos);
                setVisible(true);
                AnimManager.animAlpha(this, 0, 1);

                new Thread(() -> {
                    try {
                        Thread.sleep(5500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    dispose();
                }).start();
            });
        });
    }

    private class LinkNotificationVBox extends VBox {
        public LinkNotificationVBox(String date, String url) {
            setPrefSize(256, 64);
            setBorder(new Border(new BorderStroke(javafx.scene.paint.Color.BURLYWOOD, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(0, 2, 2, 6))));

            HBox titleHBox = new HBox(2);
            titleHBox.setBackground(new Background(new BackgroundFill(javafx.scene.paint.Color.BURLYWOOD, CornerRadii.EMPTY, javafx.geometry.Insets.EMPTY)));

            javafx.scene.control.Label titleLabel = new javafx.scene.control.Label("Shared Link");
            titleLabel.setMaxWidth(Double.MAX_VALUE);
            titleLabel.setAlignment(Pos.CENTER);
            titleLabel.setPadding(new javafx.geometry.Insets(4));
            titleLabel.setTextFill(javafx.scene.paint.Color.WHITE);
            HBox.setHgrow(titleLabel, Priority.ALWAYS);

            Separator titleSeparator = new Separator(Orientation.VERTICAL);
            titleSeparator.setPadding(new javafx.geometry.Insets(2, 0, 2, 0));
            titleSeparator.setPrefWidth(1);

            javafx.scene.control.Label dateLabel = new javafx.scene.control.Label(date);
            dateLabel.setAlignment(Pos.CENTER);
            dateLabel.setPadding(new javafx.geometry.Insets(4));
            dateLabel.setTextFill(Color.WHITE);

            titleHBox.getChildren().setAll(titleLabel, titleSeparator, dateLabel);

            javafx.scene.control.Label urlLabel = new Label(url);
            urlLabel.setWrapText(true);
            urlLabel.setPadding(new Insets(0, 2, 0, 2));


            getChildren().setAll(titleHBox, urlLabel);


        }
    }
}
