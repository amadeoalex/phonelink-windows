package amadeo.phonelink.features.linkshare;

import amadeo.phonelink.Log;
import amadeo.phonelink.features.Feature;
import amadeo.phonelink.features.FeatureManager;
import amadeo.phonelink.features.linkshare.ui.LinkNotification;
import amadeo.phonelink.network.NetPackage;
import amadeo.phonelink.ui.WindowManagerTest;
import javafx.beans.binding.Bindings;
import javafx.geometry.Insets;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Separator;
import javafx.scene.control.Tab;
import javafx.scene.layout.VBox;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;

public class LinkShareFeature extends Feature {
    private static final String TAG = "LinkShareFeature";

    private JSONObject preferences;
    private JSONArray savedLinks;

    private final String SAVE_LINKS = "saveSharedLink";
    private final String SHOW_NOTIFICATION = "showNotification";
    private final String AUTO_OPEN = "autoOpenLink";

    private final String SAVED_LINKS_FILE = "savedlinks.json";

    private File savedLinksFile;

    public LinkShareFeature() {
        super("link_", TAG);

        preferences = getPreferences();
    }

    @Override
    public void onCreate() {
        savedLinksFile = new File(getFilesPath() + SAVED_LINKS_FILE);
        Log.i(TAG, savedLinksFile.toString());
        if (savedLinksFile.exists() & !savedLinksFile.isDirectory()) {
            try {
                savedLinks = new JSONArray(new String(Files.readAllBytes(savedLinksFile.toPath())));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            savedLinks = new JSONArray();
        }

        Log.i(TAG, "Saved links: %s", savedLinks.toString());

        Log.i(TAG, "%s created", TAG);
    }

    @Override
    public void onStart() {
        Log.i(TAG, "%s started", TAG);
    }

    private void parseLink(JSONObject link) {
        if (preferences.optBoolean(SAVE_LINKS, true)) {
            savedLinks.put(link);
        }

        if (preferences.optBoolean(SHOW_NOTIFICATION, true)) {
            SwingUtilities.invokeLater(() -> {
                WindowManagerTest.getInstance().showAWTWindow("linkNotification", new LinkNotification(link.getString("date"), link.getString("url")));
            });
        }

        if (preferences.optBoolean(AUTO_OPEN, true)) {
            try {
                if (link.has("open")) {
                    URI uri = new URL(link.getString("url")).toURI();
                    Desktop.getDesktop().browse(uri);
                }
            } catch (URISyntaxException | IOException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onNetPackageReceived(NetPackage netPackage) {
        String type = netPackage.type().replaceFirst(getPrefix(), "");

        switch (type) {
            case "share":
                if (netPackage.getString("single") == null) {
                    JSONArray links = new JSONArray(netPackage.getString("json"));
                    for (int i = 0; i < links.length(); i++) {
                        JSONObject link = (JSONObject) links.get(i);
                        parseLink(link);
                    }
                } else {
                    JSONObject link = new JSONObject(netPackage.getString("json"));
                    parseLink(link);
                }
                break;
        }
    }

    @Override
    public Tab getSettingsTab() {
        Tab tab = new Tab(getName());

        VBox mainVBox = new VBox(4);
        mainVBox.setPadding(new Insets(4));

        VBox settingsVBox = new VBox(4);

        CheckBox enabledButton = new CheckBox("Enabled");
        enabledButton.setOnAction(event -> {
            if (enabledButton.isSelected()) {
                Log.i(TAG, "enabling");
                FeatureManager.enableFeature(getName());
            } else {
                Log.i(TAG, "disabling");
                FeatureManager.disableFeature(getName());
            }
        });
        enabledButton.setSelected(FeatureManager.isEnabled(getName()));

        CheckBox saveLinksButton = new CheckBox("Save received links.");
        saveLinksButton.setSelected(preferences.optBoolean(SAVE_LINKS, true));
        saveLinksButton.setOnAction(event -> {
            if (saveLinksButton.isSelected()) {
                preferences.put(SAVE_LINKS, true);
            } else {
                preferences.put(SAVE_LINKS, false);
            }
        });

        CheckBox showNotificationButton = new CheckBox("Show notification when shared link is received.");
        showNotificationButton.setSelected(preferences.optBoolean(SHOW_NOTIFICATION, true));
        showNotificationButton.setOnAction(event -> {
            if (showNotificationButton.isSelected()) {
                preferences.put(SHOW_NOTIFICATION, true);
            } else {
                preferences.put(SHOW_NOTIFICATION, false);
            }
        });

        CheckBox autoOpenButton = new CheckBox("Allow for automatic opening of shared links");
        autoOpenButton.setSelected(preferences.optBoolean(AUTO_OPEN, true));
        autoOpenButton.setOnAction(event -> {
            if (autoOpenButton.isSelected()) {
                preferences.put(AUTO_OPEN, true);
            } else {
                preferences.put(AUTO_OPEN, false);
            }
        });

        settingsVBox.getChildren().setAll(new Separator(), saveLinksButton, showNotificationButton, autoOpenButton);
        settingsVBox.disableProperty().bind(Bindings.not(enabledButton.selectedProperty()));
        mainVBox.getChildren().setAll(enabledButton, settingsVBox);

        tab.setContent(mainVBox);

        return tab;
    }

    @Override
    public void onStop() {
        if (savedLinks.length() > 0) {
            Log.i(TAG, "saving shared links");
            PrintWriter writer;
            try {
                if (!savedLinksFile.exists())
                    savedLinksFile.getParentFile().mkdirs();

                writer = new PrintWriter(savedLinksFile, "UTF-8");
                writer.print(savedLinks.toString());
                writer.flush();
                writer.close();
                Log.i(TAG, "saved");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
