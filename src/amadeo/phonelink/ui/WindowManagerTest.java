package amadeo.phonelink.ui;

import amadeo.phonelink.Log;
import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.PreferenceManager;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.json.JSONObject;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;

public class WindowManagerTest {
    private final static String TAG = "WindowManagerTest";

    private static WindowManagerTest instance;

    private Screen screen;
    private final JSONObject displaySettings;
    private HashMap<String, Stage> fxStages = new HashMap<>();
    private HashMap<String, Window> awtWindows = new HashMap<>();

    private WindowManagerTest() {
        displaySettings = PreferenceManager.getPreferences("display");
        int screenId = displaySettings.optInt("selectedScreen", 0);

        screen = Screen.getScreens().get(screenId);
    }

    public static synchronized WindowManagerTest getInstance() {
        if (instance == null)
            instance = new WindowManagerTest();

        return instance;
    }

    public void identifyScreens() {
        ObservableList<Screen> screens = Screen.getScreens();

        Platform.runLater(() -> {
            for (int i = 0; i < screens.size(); i++) {
                Stage stage = new Stage(StageStyle.TRANSPARENT);

                Label idLabel = new Label(Integer.toString(i));
                idLabel.setFont(Font.font("CALIBRI", 128));
                idLabel.setTextFill(Color.WHITE);
                idLabel.setAlignment(Pos.CENTER);
                idLabel.setBackground(new Background(new BackgroundFill(Color.TRANSPARENT, CornerRadii.EMPTY, Insets.EMPTY)));

                Scene scene = new Scene(idLabel);
                scene.setFill(Color.TRANSPARENT);
                stage.setScene(scene);

                Screen screen = screens.get(i);
//                Rectangle2D screenBounds = screen.getVisualBounds();
//                double posX = screenBounds.getMinX() + screenBounds.getWidth() / 2 - scene.getWidth() / 2 - stage.getWidth() / 2;
//                double posY = screenBounds.getMinY() + screenBounds.getHeight() / 2 - scene.getHeight() / 2 - stage.getHeight() / 2;

                stage.getIcons().setAll(PhoneLinkUtils.icon);
                stage.show();
                WindowManagerTest.getInstance().centerFXStageOnScreen(screen, stage);

                System.out.printf("W: %f, H: %f\n", screen.getVisualBounds().getWidth(), screen.getVisualBounds().getHeight());

                new Thread(() -> {
                    try {
                        Thread.sleep(2000);
                        Platform.runLater(stage::close);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }).start();
            }
        });
    }

    public Screen getScreen() {
        return screen;
    }

    public void setScreen(int screenId) {
        displaySettings.put("selectedScreen", screenId);
        this.screen = Screen.getScreens().get(screenId);
    }

    private void centerFXStageOnCurrentScreen(Stage stage) {
        centerFXStageOnScreen(this.screen, stage);
    }

    private void centerFXStageOnScreen(Screen screen, Stage stage) {
        Rectangle2D screenBounds = screen.getVisualBounds();
        double posX = screenBounds.getMinX() + screenBounds.getWidth() / 2 - stage.getWidth() / 2;
        double posY = screenBounds.getMinY() + screenBounds.getHeight() / 2 - stage.getHeight() / 2;

        stage.setX(posX);
        stage.setY(posY);
    }

    public void showFXStage(String key, Stage stage) {
        Log.w(TAG, "creating stage %s", key);

        Platform.runLater(() -> {
            if (fxStages.containsKey(key)) {
                Log.w(TAG, "stage %s already present, requesting focus", key);
                fxStages.get(key).requestFocus();

                //TODO: creating and closing stuff is not efficient...
                stage.close();

                return;
            }

            stage.setOnHidden(event -> {
                if (fxStages.containsKey(key)) {
                    if (fxStages.get(key) != null)
                        fxStages.get(key).close();

                    fxStages.remove(key);
                }
            });
            ;

            fxStages.put(key, stage);
            stage.show();
            centerFXStageOnCurrentScreen(stage);
        });
    }

    public void showAWTWindow(String key, Window window) {
        SwingUtilities.invokeLater(() -> {
            if (awtWindows.containsKey(key)) {
                Log.w(TAG, "window %s is already present, closing older one", key);
                awtWindows.get(key).dispose();
            }

            window.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    super.windowClosed(e);

                    awtWindows.remove(key);
                }
            });

            awtWindows.put(key, window);
            window.setVisible(true);
        });
    }

    public Stage getFXStage(String key) {
        Log.w(TAG, "lookup for stage %s", key);
        return fxStages.getOrDefault(key, null);
    }

    public boolean hasStage(String key) {
        return fxStages.containsKey(key);
    }

    public Window getAWTWindow(String name) {
        return awtWindows.getOrDefault(name, null);
    }

    public boolean hasATWWindow(String key) {
        return awtWindows.containsKey(key);
    }

    public void cleanup() {
        for (Stage stage : fxStages.values())
            stage.close();

        for (Window window : awtWindows.values())
            window.dispose();
    }
}
