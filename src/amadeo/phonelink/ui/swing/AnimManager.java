package amadeo.phonelink.ui.swing;

import java.awt.*;

enum TYPE {
    POSITION,
    ALPHA
}

public class AnimManager {
    public static void animFromTo(Window window, int xFrom, int yFrom, int xTo, int yTo, int speedMillis) {
        new Animator(window, xFrom, yFrom, xTo, yTo, speedMillis).start();
    }

    public static void animFromTo(Window window, int xFrom, int yFrom, int xTo, int yTo) {
        AnimManager.animFromTo(window, xFrom, yFrom, xTo, yTo, 4);
    }

    public static void animAlpha(Window window, float alphaFrom, float alphaTo, int speedMillis, float step) {
        new Animator(window, alphaFrom, alphaTo, speedMillis, step).start();
    }

    public static void animAlpha(Window window, float alphaFrom, float alphaTo) {
        AnimManager.animAlpha(window, alphaFrom, alphaTo, 1, 0.001f);
    }

    private static class Animator extends Thread {
        private final int speedMillis;
        private float step;

        private int xFrom, yFrom;
        private int xTo, yTo;
        private Window window;

        private float alphaFrom;
        private float alphaTo;

        private TYPE type;

        public Animator(Window window, int xFrom, int yFrom, int xTo, int yTo, int speedMillis) {
            this.xFrom = xFrom;
            this.yFrom = yFrom;
            this.xTo = xTo;
            this.yTo = yTo;
            this.window = window;
            this.speedMillis = speedMillis;
            this.type = TYPE.POSITION;
        }

        public Animator(Window window, float alphaFrom, float alphaTo, int speedMillis, float step) {
            this.window = window;
            this.alphaFrom = alphaFrom;
            this.alphaTo = alphaTo;
            this.speedMillis = speedMillis;
            this.step = step;
            this.type = TYPE.ALPHA;
        }

        @Override
        public void run() {
            long lastMillis = 0;

            if (type == TYPE.POSITION) {
                int xSign = (int) Math.signum(xTo - xFrom);
                int ySign = (int) Math.signum(yTo - yFrom);
                while (xFrom != xTo || yFrom != yTo) {
                    long millis = System.currentTimeMillis();
                    if (millis >= (lastMillis + speedMillis)) {
                        window.setLocation(xFrom += xSign, yFrom += ySign);
                        lastMillis = millis;
                    }
                }
            } else if (type == TYPE.ALPHA) {
                float signedStep = Math.signum(alphaTo - alphaFrom) * step;
                while (alphaFrom <= alphaTo) {
                    long millis = System.currentTimeMillis();
                    if (millis >= lastMillis + speedMillis) {
                        window.setOpacity(Math.max(0, Math.min(1, alphaFrom+=signedStep)));
                        lastMillis = millis;
                    }
                }
            }
        }

    }
}