package amadeo.phonelink.ui;

import amadeo.phonelink.PhoneLink;
import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.network.manager.ConnectionManager;
import amadeo.phonelink.network.NetPackage;
import amadeo.phonelink.network.manager.PairingThread;
import amadeo.phonelink.network.manager.UDPServer;
import amadeo.phonelink.ui.fxml.pairing.PairingStage;
import amadeo.phonelink.ui.fxml.settings.SettingsStage;
import javafx.application.Platform;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.IOException;
import java.net.*;

public class TrayManager {

    public static void setupTray() {
        SystemTray tray = SystemTray.getSystemTray();

        Image image = null;
        try {
            image = ImageIO.read(PhoneLink.class.getResource("resources/image/tray_icon.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        TrayIcon trayIcon = new TrayIcon(image);
        trayIcon.setImageAutoSize(true);

        MenuItem settingsItem = new MenuItem("Settings");
        settingsItem.addActionListener(e -> {
            Platform.runLater(() -> WindowManagerTest.getInstance().showFXStage("settings", new SettingsStage()));
        });

        MenuItem testItem = new MenuItem("test");
        testItem.addActionListener(e -> {
//            Platform.runLater(() -> {
////                Stage stage = new ConversationStage("123");
////
////                WindowManagerTest.getInstance().showFXStage("123", stage);
//                ConnectionManager.getConnectionServer().sendNetPackage(new NetPackage("sms_blaah"));
//            });
            try {
                NetPackage pairRequestPackage = new NetPackage(UDPServer.TYPE_PAIR);
                pairRequestPackage.setString("deviceID", PhoneLinkUtils.getDeviceName());
                pairRequestPackage.setInt("tcpPort", 6162); //TODO: randomize

                new PairingThread(InetAddress.getByName("127.0.0.1"), pairRequestPackage, ConnectionManager.ConnectionOrigin.Outgoing).start();

                byte[] buffer = pairRequestPackage.toString().getBytes();
                DatagramPacket pairRequestPacket = new DatagramPacket(buffer, buffer.length, InetAddress.getByName("127.0.0.1"), 4849);

                DatagramSocket datagramSocket = new DatagramSocket();
                datagramSocket.send(pairRequestPacket);

            } catch (UnknownHostException e1) {
                e1.printStackTrace();
            } catch (SocketException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });

        MenuItem pairItem = new MenuItem("Pair");
        pairItem.addActionListener(event -> {
            if (!WindowManagerTest.getInstance().hasStage("pairing"))
                Platform.runLater(() -> WindowManagerTest.getInstance().showFXStage("pairing", new PairingStage()));
        });

        MenuItem exitItem = new MenuItem("Exit");
        exitItem.addActionListener(event -> {
            Platform.exit();
            tray.remove(trayIcon);
        });

        MenuItem disconnectItem = new MenuItem("Disconnect");
        disconnectItem.addActionListener(event -> {
            ConnectionManager.getConnectionServer().disconnect();
        });

        PopupMenu popupMenu = new PopupMenu();
        popupMenu.add(settingsItem);
        popupMenu.add(testItem);
        popupMenu.add(pairItem);
        popupMenu.addSeparator();
        popupMenu.add(exitItem);
        popupMenu.add(disconnectItem);

        trayIcon.setPopupMenu(popupMenu);

        try {
            tray.add(trayIcon);
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }
}
