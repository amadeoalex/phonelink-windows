package amadeo.phonelink.ui.fxml.settings;

import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.features.Feature;
import amadeo.phonelink.features.FeatureManager;
import amadeo.phonelink.ui.WindowManagerTest;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.util.concurrent.ConcurrentHashMap;

public class SettingsStage extends Stage {
    public SettingsStage() {
        setTitle("Settings");
        getIcons().setAll(PhoneLinkUtils.icon);

        Scene scene = new Scene(new SettingsVBox());
        setScene(scene);
    }

    private class SettingsVBox extends VBox {
        public SettingsVBox() {
            super(4);

            TabPane mainTabPane = new TabPane();
            mainTabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);

            Tab generalTab = new Tab("General Settings");
            Tab displayTab = new Tab("Display Settings");
            Tab featuresTab = new Tab("Feature Settings");

            displayTab.setContent(prepareDisplayTabContent());

            mainTabPane.getTabs().addAll(generalTab, displayTab, featuresTab);


            TabPane featuresTabPane = new TabPane();
            featuresTabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);

            ConcurrentHashMap features = FeatureManager.getFeatures();
            for (Object f : features.values()) {
                Feature feature = (Feature) f;
                featuresTabPane.getTabs().add(feature.getSettingsTab());
            }

            featuresTab.setContent(featuresTabPane);

            getChildren().setAll(mainTabPane);
        }

        private VBox prepareDisplayTabContent() {
            VBox mainVBox = new VBox(4);
            mainVBox.setPadding(new Insets(4));

            Label screenChoiceLabel = new Label("Select screen:");

            ChoiceBox choiceBox = new ChoiceBox();
            ObservableList<Screen> screens = Screen.getScreens();
            choiceBox.setItems(screens);
            choiceBox.getSelectionModel().select(WindowManagerTest.getInstance().getScreen());
            choiceBox.setMaxWidth(256);

            choiceBox.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> WindowManagerTest.getInstance().setScreen(newValue.intValue()));

            HBox displaySelectHBox = new HBox(4);
            displaySelectHBox.setAlignment(Pos.CENTER_LEFT);
            displaySelectHBox.getChildren().setAll(screenChoiceLabel, choiceBox);

            Button identifyScreensButton = new Button("Identify Screens");
            identifyScreensButton.setOnAction(event -> {
                WindowManagerTest.getInstance().identifyScreens();
            });

            mainVBox.getChildren().setAll(displaySelectHBox, identifyScreensButton);

            return mainVBox;
        }
    }

}
