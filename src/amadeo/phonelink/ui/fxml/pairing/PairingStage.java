package amadeo.phonelink.ui.fxml.pairing;

import amadeo.phonelink.Log;
import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.network.manager.PairingThread;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

public class PairingStage extends Stage {
    private final String TAG = "PairingStage";
    private final PairingVBox pairingVBox;

    //TODO: remove this class
    public PairingStage() {
        setTitle("Pairing");
        getIcons().setAll(PhoneLinkUtils.icon);

        pairingVBox = new PairingVBox();
        Scene scene = new Scene(pairingVBox);
        setScene(scene);
    }

    @Override
    public void close() {
        super.close();

        pairingVBox.onClose();
    }

    private class PairingVBox extends VBox {
        private Label instructionLabel;
        private PairingThread pairingThread;

        public PairingVBox() {
            super(4);
            setId("scene");

            setPrefSize(256, 128);

            instructionLabel = new Label("Please open PhoneLink app on your Android device and begin pairing process");
            instructionLabel.setWrapText(true);
            instructionLabel.setTextAlignment(TextAlignment.CENTER);

            setPadding(new Insets(4));
            setAlignment(Pos.CENTER);
            getChildren().setAll(instructionLabel);

            //pairingThread = new PairingThread(instructionLabel);
            //pairingThread.start();
        }

        public void onClose(){
            pairingThread.interrupt();
            try {
                pairingThread.join();
                Log.i(TAG, "pairingThread joined");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
