package amadeo.phonelink;

public class Log {
    private static boolean logEnabled = false;
    private static String filter = null;

    public static void setLogEnabled() {
        logEnabled = true;
    }

    public static void setFilter(String filter) {
        Log.filter = filter;
    }

    public static void i(String TAG, String message, Object... objects) {
        if (logEnabled) {
            if (filter != null && !TAG.equals(filter))
                return;

            String taggedMessage = String.format("[" + TAG + "]: " + message, objects);
            System.out.println(taggedMessage);
        }
    }

    public static void w(String TAG, String message, Object... objects) {
        if (logEnabled) {
            if (filter != null && !TAG.equals(filter))
                return;

            String taggedMessage = String.format("[" + TAG + "]: " + message, objects);
            System.err.println(taggedMessage);
        }
    }
}
