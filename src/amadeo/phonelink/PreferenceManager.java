package amadeo.phonelink;


import amadeo.phonelink.device.Phone;
import org.json.JSONObject;


import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;

public class PreferenceManager {
    private static final String TAG = "PreferenceManger";

    private static final String PREFERENCE_FILE = "pref.json";
    private static JSONObject preferences = loadPreferences();
    private static JSONObject defaultPreferences = new JSONObject(PhoneLinkUtils.loadResourceAsString("resources/json/default_preferences.json"));

    private static final byte[] iv = new byte[]{
            (byte) 0x8E, 0x12, 0x39, (byte) 0x9C, 0x07, 0x72, 0x6F, 0x5A,
            (byte) 0x8E, 0x12, 0x39, (byte) 0x9C, 0x07, 0x72, 0x6F, 0x5A
    };

    private static boolean debugMode = false;

    private static JSONObject loadPreferences() {
        JSONObject object = null;

        try {
            String content = new String(Files.readAllBytes(Paths.get(PREFERENCE_FILE)));
            object = new JSONObject(content);
        } catch (NoSuchFileException e) {
            //TODO change this: find out if file exist first...
            object = new JSONObject();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return object;
    }

    public static void debugMode() {
        debugMode = true;

        Log.i(TAG, "debug mode!");
        try {
            setPhone(new Phone("debugID", iv, InetAddress.getByName("127.0.0.1"), 7878, 6868));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public static boolean isDebugMode() {
        return debugMode;
    }

    public static void setPhone(Phone phone) {
        preferences.put("phone", phone.toString());
    }

    public static Phone getPhone() {
        String serialized = preferences.optString("phone", null);
        if (serialized != null)
            return Phone.deserialize(serialized);
        else
            return null;
    }

    public static void savePreferences() {
        try (FileWriter file = new FileWriter(PREFERENCE_FILE)) {
            file.write(preferences.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static JSONObject getPreferences(String key) {
        if (preferences.has(key)) {
            return preferences.getJSONObject(key);
        } else {
            preferences.put(key, new JSONObject());
            return preferences.getJSONObject(key);
        }
    }
}
