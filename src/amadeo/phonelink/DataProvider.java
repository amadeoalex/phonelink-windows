package amadeo.phonelink;

import amadeo.phonelink.features.smsnotifications.*;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class DataProvider {
    private static final String TAG = "DataProvider";
    private static ConcurrentHashMap<String, Conversation> conversations = new ConcurrentHashMap<>();
    private static CopyOnWriteArrayList<String> contacts = preloadData();

    private static CopyOnWriteArrayList<String> preloadData() {
        CopyOnWriteArrayList<String> contacts = new CopyOnWriteArrayList<>();
        File directory = new File(".");
        for (File f : directory.listFiles()) {
            String fName = f.getName().toLowerCase();
            if (fName.endsWith(".con"))
                contacts.add(fName.substring(0, fName.lastIndexOf('.')));
        }

        return contacts;
    }

    public static List<String> getContacts() {
        return contacts;
    }

    private static synchronized void loadData(String number) {
        try {
            String fileName = number + ".con";
            File file = new File(fileName);

            if (file.exists() && !file.isDirectory()) {
                String fileContent = new String(Files.readAllBytes(Paths.get(fileName)));

                JSONObject jsonObject = new JSONObject(fileContent);
                conversations.put(number, new Conversation(jsonObject));
            } else {
                Log.i(TAG, "No conv with number: %s", number);
                conversations.put(number, new Conversation(number));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static synchronized Conversation getConversation(String number) {
        if (!contacts.contains(number))
            contacts.add(number);

        if (conversations.containsKey(number))
            return conversations.get(number);

        loadData(number);
        return conversations.get(number);
    }

    public static synchronized void addToConversation(String number, String name, String data, String date) {
        Conversation conversation = getConversation(number);
        JSONObject message = new JSONObject();
        message.put("name",name).put("body",data).put("date",date);
        conversation.addMessage(message);
    }

    public static synchronized void addToConversation(SMS sms) {
        addToConversation(sms.number, sms.name, sms.body, sms.date);
    }

    public static synchronized void addToConversation(String number, JSONArray messages) {
        Conversation conversation = getConversation(number);

        //That's dirty, ugly and terrible - CHANGE IT YOU TWAT
        JSONObject lastMessage = conversation.getLastMessage();
        JSONObject newestMessage = messages.getJSONObject(messages.length() - 1);
        if (lastMessage.get("date").equals(newestMessage.get("date")) && lastMessage.get("body").equals(newestMessage.get("body"))) {
            conversation.removeLastMessage();
        }

        for (Object o : messages)
            conversation.addMessage((JSONObject) o);
    }

    public static synchronized void saveConversations() {
        for (Conversation c : conversations.values()) {
            String fileName = c.getNumber() + ".con";

            try {
                FileWriter fileWriter = new FileWriter(fileName);
                fileWriter.write(c.toString());
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
