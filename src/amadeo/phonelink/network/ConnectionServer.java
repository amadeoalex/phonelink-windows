package amadeo.phonelink.network;

import amadeo.phonelink.Log;
import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.PreferenceManager;
import amadeo.phonelink.SecurityUtils;
import amadeo.phonelink.features.FeatureManager;
import amadeo.phonelink.device.Phone;

import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.*;
import java.net.*;
import java.util.concurrent.LinkedBlockingDeque;

public class ConnectionServer extends Thread implements ConnectionCallback {
    private static final String TAG = "ConnectionServer";

    private volatile SSLSocket sslSocket;
    private volatile BufferedReader bufferedReader;
    private volatile BufferedWriter bufferedWriter;

    private ConnectionListener connectionListener;
    private PackageListener packageListener;

    private LinkedBlockingDeque<NetPackage> netPackages = new LinkedBlockingDeque<>();

    private final Phone phone;

    public ConnectionServer() {
        //TODO: change this...
        this.phone = PreferenceManager.getPhone();
    }


    @Override
    public void onConnected(SSLSocket newSocket) {
        sslSocket = newSocket;

        if (sslSocket != null) {
            try {
                bufferedWriter = new BufferedWriter(new OutputStreamWriter(sslSocket.getOutputStream(), "UTF-8"));
                bufferedReader = new BufferedReader(new InputStreamReader(sslSocket.getInputStream(), "UTF-8"));

                if (packageListener != null && packageListener.isAlive()) {
                    Log.w(TAG, "PackageListener should be null or not alive!");

                    try {
                        throw new Exception("PackageListener should be null or not alive!");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                packageListener = new PackageListener(this, bufferedReader);
                packageListener.start();

                FeatureManager.onConnected();
            } catch (SSLHandshakeException e) {
                Log.i(TAG, "ssl handshake exception, pair link is no longer valid?");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void startListener() {
        if (connectionListener != null && connectionListener.isAlive()) {
            Log.i(TAG, "ConnectionListener is either null or alive!");

            try {
                throw new Exception("ConnectionListener is either nonnull or alive!");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!ConnectionServer.this.isInterrupted()) {
            connectionListener = new ConnectionListener(this, phone, sslSocket);
            connectionListener.start();
        } else {
            Log.i(TAG, "thread is interrupted, ConnectionListener not created");
        }
    }

    @Override
    public void run() {
        startListener();

        while (!ConnectionServer.this.isInterrupted()) {
            NetPackage netPackage = null;

            try {
                netPackage = netPackages.take();

                if (sslSocket == null || sslSocket.isClosed()) {
                    Log.i(TAG, "socket is null, trying to connect to %s", phone.getLastKnownPCAddress().toString());
                    //Socket newSocket = new Socket(phone.getLastKnownPCAddress(), phone.getTcpPort());
                    SSLSocketFactory sslSocketFactory = SecurityUtils.getSSLContext(phone).getSocketFactory();
                    //SSLSocket newSocket = (SSLSocket) sslSocketFactory.createSocket(phone.getLastKnownPCAddress(), phone.getTcpPort());
                    SSLSocket newSocket = (SSLSocket) sslSocketFactory.createSocket();
                    newSocket.connect(new InetSocketAddress(phone.getLastKnownPCAddress(), phone.getTcpPort()), 1000);

                    connectionListener.interrupt();
                    onConnected(newSocket);
                }

                Log.i(TAG, "sending: %s", netPackage.type());
                bufferedWriter.write(netPackage.toString());
                bufferedWriter.newLine();
                bufferedWriter.flush();
                Log.i(TAG, "sent");
            } catch (InterruptedException e) {
                Log.i(TAG, "take() interrupted, killing thread");
                break;
            } catch (ConnectException e) {
                Log.i(TAG, "ConnectException: %s, ignoring netPackage: %s", e.getMessage(), netPackage.type());
                FeatureManager.failedNetPackage(netPackage);
            } catch (SocketTimeoutException e) {
                Log.i(TAG, "socket timeout exception");
            } catch (StreamCorruptedException e) {
                Log.i(TAG, "stream is corrupted, restarting socket and trying again: %s", e.getMessage());

                try {
                    sslSocket.close();
                    sslSocket = null;
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                netPackages.addFirst(netPackage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Log.i(TAG, "thread finished");
    }

    @Override
    public void interrupt() {
        super.interrupt();

        try {
            if (sslSocket != null)
                sslSocket.close();

            Log.i(TAG, "joining PackageListener");
            if (packageListener != null && packageListener.isAlive()) {
                packageListener.interrupt();
                packageListener.join();
            }

            Log.i(TAG, "joining ConnectionListener");
            if (connectionListener != null && connectionListener.isAlive()) {
                connectionListener.interrupt();
                connectionListener.join();
            }


        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void sendNetPackage(NetPackage netPackage) {
        netPackages.add(netPackage);
    }

    public void sendPriorityNetPackage(NetPackage netPackage) {
        netPackages.addFirst(netPackage);
    }

    public void disconnect() {
        Log.i(TAG, "closing socket");
        try {
            if (sslSocket != null) {
                sslSocket.shutdownInput();
                sslSocket.shutdownOutput();
                sslSocket.close();
            } else
                Log.i(TAG, "socket is null");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "socket closed");
    }
}
