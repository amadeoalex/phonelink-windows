package amadeo.phonelink.network;

import amadeo.phonelink.Log;
import amadeo.phonelink.features.FeatureManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.ObjectInputStream;

public class PackageListener extends Thread {
    private final String TAG = "PackageListener";

    private final ConnectionCallback connectionCallback;
    private final BufferedReader bufferedReader;

    public PackageListener(ConnectionCallback connectionCallback, BufferedReader bufferedReader) {
        this.connectionCallback = connectionCallback;
        this.bufferedReader = bufferedReader;
    }

    @Override
    public void run() {
        Log.i(TAG, "started");

        while (!Thread.currentThread().isInterrupted()) {
            try {
                Log.i(TAG, "waiting for line");
                String jsonString = bufferedReader.readLine();
                if(jsonString==null)
                    throw new IOException("json is null");

                Log.i(TAG, "deserializing");
                NetPackage netPackage = NetPackage.deserialize(jsonString);
                Log.i(TAG, "package: %s", netPackage.type());

                Log.i(TAG, "parsing");
                FeatureManager.parseNetPackage(netPackage);
            } catch (IOException | NullPointerException e) {
                Log.w(TAG, "connection problem, resetting: %s", e.getMessage());

//                //TODO: find better way for socket closed recognition
                  //TODO: show notification when ValidatorException occurs
//                if (!e.equals("Socket closed"))
                    Thread.currentThread().interrupt();

                connectionCallback.startListener();
            }
        }

        Log.i(TAG, "thread finished");
    }

    @Override
    public void interrupt() {
        super.interrupt();

        Log.i(TAG, "interrupted");
    }
}
