package amadeo.phonelink.network;

import javax.net.ssl.SSLSocket;
import java.net.Socket;

public interface ConnectionCallback {
    void onConnected(SSLSocket newSocket);

    void startListener();
}
