package amadeo.phonelink.network.manager;

import amadeo.phonelink.Log;
import amadeo.phonelink.PreferenceManager;
import amadeo.phonelink.network.ConnectionServer;
import amadeo.phonelink.network.NetPackage;

import java.net.Socket;

public class ConnectionManager {
    private static final String TAG = "ConnectionManager";

    private static UDPServer udpServer;
    private static ConnectionServer connectionServer;

    public enum ConnectionOrigin {
        Outgoing, Incomming
    }


    public static void startBeaconServer() {
        udpServer = new UDPServer();
        udpServer.start();
    }

    public static void startConnectionServer() {
        connectionServer = new ConnectionServer();
        connectionServer.start();
    }

    public static void start() {
        startBeaconServer();
        if (PreferenceManager.getPhone() != null) {
            startConnectionServer();
        } else {
            Log.i(TAG, "ConnectionServer not started, not paired to any phone.");
        }
    }

    public static void cleanup() {
        try {
            if (udpServer != null) {
                Log.i(TAG, "joining udpServer");
                udpServer.interrupt();
                udpServer.join();
                Log.i(TAG, "udpServer joined");
            }
            if (connectionServer != null) {
                Log.i(TAG, "joining connectionServer");
                connectionServer.interrupt();
                connectionServer.join();
                Log.i(TAG, "connectionServer joined");
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static ConnectionServer getConnectionServer() {
        return connectionServer;
    }

    public static void restartConnectionServer() {
        if (connectionServer != null) {
            try {
                connectionServer.interrupt();
                connectionServer.join();
                connectionServer = null;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        startConnectionServer();
        Log.i(TAG, "Connection server restarted");
    }
}
