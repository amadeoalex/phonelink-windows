package amadeo.phonelink.network.manager;

import amadeo.phonelink.Log;
import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.PreferenceManager;
import amadeo.phonelink.SecurityUtils;
import amadeo.phonelink.device.Phone;
import amadeo.phonelink.network.NetPackage;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;

import javax.crypto.*;
import javax.crypto.interfaces.DHPublicKey;
import javax.crypto.spec.DHParameterSpec;
import javax.crypto.spec.DHPublicKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.*;
import java.io.*;
import java.math.BigInteger;
import java.net.*;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;

public class PairingThread extends Thread {
    private static final String TAG = "PT";
    private InetAddress inetAddress;
    private NetPackage netPackage;
    private ConnectionManager.ConnectionOrigin origin;

    public PairingThread(InetAddress inetAddress, NetPackage netPackage, ConnectionManager.ConnectionOrigin origin) {
        this.inetAddress = inetAddress;
        this.netPackage = netPackage;
        this.origin = origin;

        Security.addProvider(new BouncyCastleProvider());
    }

    @Override
    public void run() {
        //TODO: refactor this code/cleanup/repeating fragments of code
        if (origin == ConnectionManager.ConnectionOrigin.Incomming) {
            SSLSocket sslSocket = null;
            try {
                Log.i(TAG, "trying to connect to %s at port %d", inetAddress.getHostAddress(), netPackage.getInt("tcpPort"));
                sslSocket = (SSLSocket) SecurityUtils.getSSLContext(null).getSocketFactory().createSocket(inetAddress, netPackage.getInt("tcpPort")); //debug:6162
                sslSocket.startHandshake();

                PrintWriter printWriter = new PrintWriter(sslSocket.getOutputStream(), true);
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(sslSocket.getInputStream()));

                //Send my identity package
                NetPackage deviceIDPackage = getMyIDNetPackage();
                printWriter.println(deviceIDPackage.toString());

                //Get remote device identity package
                String identityPackage = bufferedReader.readLine();
                NetPackage remoteDeviceIDPackage = NetPackage.deserialize(identityPackage);
                //System.out.println(remoteDeviceIDPackage);

                String remoteDeviceID = remoteDeviceIDPackage.getString("deviceID");
                byte[] remoteDeviceEncodedCertificate = Base64.getDecoder().decode(remoteDeviceIDPackage.getString("certificate"));
                InetAddress remoteDeviceInetAddress = sslSocket.getInetAddress();
                int remoteDeviceTCPPort = remoteDeviceIDPackage.getInt("tcpPort");
                int remoteDeviceLTCPPort = remoteDeviceIDPackage.getInt("listeningTCPPort");

                Phone phone = new Phone(remoteDeviceID, remoteDeviceEncodedCertificate, remoteDeviceInetAddress, remoteDeviceTCPPort, remoteDeviceLTCPPort);
                PreferenceManager.setPhone(phone);
                ConnectionManager.restartConnectionServer();

                sslSocket.close();
            } catch (ConnectException e) {
                Log.i(TAG, "connect exception: %s", e.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (CertificateEncodingException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (sslSocket != null && !sslSocket.isClosed())
                        sslSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            SSLServerSocket sslServerSocket = null;
            SSLSocket sslSocket = null;

            try {
                Log.i(TAG, "creating server at port %d", netPackage.getInt("tcpPort"));
                SSLServerSocketFactory sslServerSocketFactory = SecurityUtils.getSSLContext(null).getServerSocketFactory();
                sslServerSocket = (SSLServerSocket) sslServerSocketFactory.createServerSocket(netPackage.getInt("tcpPort")); //debug:7171
                sslServerSocket.setSoTimeout(8000);

                sslSocket = (SSLSocket) sslServerSocket.accept();
                sslSocket.startHandshake();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(sslSocket.getInputStream()));
                PrintWriter printWriter = new PrintWriter(sslSocket.getOutputStream(), true);

                //Get remote device identity package
                String identityString = bufferedReader.readLine();
                NetPackage remoteDeviceIDPackage = NetPackage.deserialize(identityString);

                //Send my identity package
                NetPackage deviceIDPackage = getMyIDNetPackage();
                printWriter.println(deviceIDPackage.toString());

                String remoteDeviceID = remoteDeviceIDPackage.getString("deviceID");
                byte[] remoteDeviceEncodedCertificate = Base64.getDecoder().decode(remoteDeviceIDPackage.getString("certificate"));
                InetAddress remoteDeviceInetAddress = sslSocket.getInetAddress();
                int remoteDeviceTCPPort = remoteDeviceIDPackage.getInt("tcpPort");
                int remoteDeviceLTCPPort = remoteDeviceIDPackage.getInt("listeningTCPPort");

                Phone phone = new Phone(remoteDeviceID, remoteDeviceEncodedCertificate, remoteDeviceInetAddress, remoteDeviceTCPPort, remoteDeviceLTCPPort);
                PreferenceManager.setPhone(phone);
                ConnectionManager.restartConnectionServer();
            } catch (SocketTimeoutException e) {
                Log.w(TAG, "socket timeout exception");
            } catch (IOException e) {
                e.printStackTrace();
            } catch (CertificateEncodingException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (sslServerSocket != null && !sslServerSocket.isClosed())
                        sslServerSocket.close();

                    if (sslSocket != null && sslSocket.isClosed())
                        sslSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private NetPackage getMyIDNetPackage() throws CertificateEncodingException {
        NetPackage deviceIDPackage = new NetPackage("");
        deviceIDPackage.setString("deviceID", PhoneLinkUtils.getDeviceName());
        deviceIDPackage.setInt("tcpPort", 9999);
        deviceIDPackage.setInt("listeningTCPPort", 8888);
        deviceIDPackage.setString("certificate", Base64.getEncoder().encodeToString(SecurityUtils.getDeviceCertificate().getEncoded()));

        return deviceIDPackage;
    }
}
