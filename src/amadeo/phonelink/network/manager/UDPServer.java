package amadeo.phonelink.network.manager;

import amadeo.phonelink.Log;
import amadeo.phonelink.PhoneLink;
import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.PreferenceManager;
import amadeo.phonelink.network.NetPackage;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Socket;

public class UDPServer extends Thread {
    private static final String TAG = "UDPServer";

    public static final String TYPE_PAIR = "amadeo.phonelink.package.PAIR";
    public static final String TYPE_LOOKUP = "amadeo.phonelink.package.LOOKUP";
    public static final String TYPE_LOOKUP_RESPONSE = "amadeo.phonelink.package.LOOKUP_RESPONSE";

    private DatagramSocket serverSocket;
    private boolean running = true;

    public UDPServer() {
        Log.i(TAG, "UDPServer constructor.");
    }

    @Override
    public void run() {
        try {
            serverSocket = new DatagramSocket(4848);

            while (running) {
                byte[] buffer = new byte[1024 * 256];
                DatagramPacket receivedPacket = new DatagramPacket(buffer, buffer.length);

                serverSocket.receive(receivedPacket);
                DatagramPacket responsePacket = packetReceived(receivedPacket);

                if (responsePacket != null)
                    serverSocket.send(responsePacket);
            }

            serverSocket.close();
        } catch (IOException e) {
            if (running)
                e.printStackTrace();
        }
    }

    private DatagramPacket packetReceived(DatagramPacket packet) {
        NetPackage netPackage = NetPackage.deserialize(new String(packet.getData()).trim());

        String receivedID = netPackage.getString("deviceID");
        if (receivedID != null && receivedID.equals(PhoneLinkUtils.getDeviceName()))
            return null;

        switch (netPackage.type()) {
            case TYPE_LOOKUP:
                Log.i(TAG, "lookup package received from: %s", packet.getAddress().getHostAddress());
                NetPackage responseNetPackage = new NetPackage(TYPE_LOOKUP_RESPONSE);
                responseNetPackage.setString("deviceID", PhoneLinkUtils.getDeviceName());

                return new DatagramPacket(responseNetPackage.toString().getBytes(), responseNetPackage.toString().getBytes().length, packet.getAddress(), packet.getPort());
            case TYPE_PAIR:
                Log.i(TAG, "pair package received from %s", packet.getAddress().getHostAddress());

                if (PreferenceManager.isDebugMode() && netPackage.getInt("tcpPort") == 6162)
                    netPackage.setInt("tcpPort", 6161);

                new PairingThread(packet.getAddress(), netPackage, ConnectionManager.ConnectionOrigin.Incomming).start();

                return null;
            default:
                Log.w(TAG, "unhandled package received: %s", netPackage.type());

                return null;
        }
    }


    @Override
    public void interrupt() {
        running = false;
        serverSocket.close();
    }


}