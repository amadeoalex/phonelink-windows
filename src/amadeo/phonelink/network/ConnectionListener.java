package amadeo.phonelink.network;

import amadeo.phonelink.Log;
import amadeo.phonelink.PhoneLinkUtils;
import amadeo.phonelink.PreferenceManager;
import amadeo.phonelink.SecurityUtils;
import amadeo.phonelink.device.Phone;

import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLSocket;
import java.io.*;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class ConnectionListener extends Thread {
    private final String TAG = "ConnectionListener";

    private final ConnectionCallback connectionCallback;

    private SSLServerSocket serverSocket;
    private final Phone phone;

    public ConnectionListener(ConnectionCallback connectionCallback, Phone phone, Socket oldSocket) {
        this.connectionCallback = connectionCallback;
        this.phone = phone;

        try {
            if (oldSocket != null) {
                oldSocket.close();
            }

            serverSocket = (SSLServerSocket) SecurityUtils.getSSLContext(phone).getServerSocketFactory().createServerSocket();
            serverSocket.setReuseAddress(true);
            serverSocket.setNeedClientAuth(true);
            serverSocket.bind(new InetSocketAddress(phone.getListeningTCPPort()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        Log.i(TAG, "started at port %d", phone.getListeningTCPPort());

        try {
            SSLSocket newSocket = (SSLSocket) serverSocket.accept();
            serverSocket.close();
            connectionCallback.onConnected(newSocket);
        } catch (IOException e) {
            if (!interrupted())
                e.printStackTrace();
        }

        Log.i(TAG, "thread finished");
    }

    @Override
    public void interrupt() {
        super.interrupt();

        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
