package amadeo.phonelink.network;

import amadeo.phonelink.Log;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class NetPackage {
    private final String TAG = "NetPackage";
    private JSONObject jsonObject;

    public NetPackage() {
    }

    public NetPackage(String type) {
        jsonObject = new JSONObject();
        this.jsonObject.put("__type__", type);
    }


    public static NetPackage deserialize(String serialized) {
        JSONObject jsonObject = new JSONObject(serialized);

        NetPackage netPackage = new NetPackage();
        netPackage.jsonObject = jsonObject;

        return netPackage;
    }

    @Deprecated
    public NetPackage(BufferedReader bufferedReader) throws IOException, NullPointerException {
        String jsonString = bufferedReader.readLine();

        this.jsonObject = new JSONObject(jsonString);
    }

    public void setType(String type) {
        this.jsonObject.put("__type__", type);
    }

    public String type() {
        return this.jsonObject.getString("__type__");
    }

    public void setString(String key, String value) {
        this.jsonObject.put(key, value);
    }

    public String getString(String key) {
        return this.jsonObject.optString(key, null);
    }

    public void setJSONObject(String key, JSONObject jsonObject) {
        this.jsonObject.put(key, jsonObject);
    }

    public JSONObject getJSONObject(String key) {
        return this.jsonObject.getJSONObject(key);
    }

    public void setJSONArray(String key, JSONArray jsonArray) {
        this.jsonObject.put(key, jsonArray);
    }

    public JSONArray getJSONArray(String key) {
        return this.jsonObject.getJSONArray(key);
    }

    public void setInt(String key, int integer) {
        this.jsonObject.put(key, integer);
    }

    public int getInt(String key) {
        return this.jsonObject.getInt(key);
    }

    public int size() {
        return jsonObject.length() - 1;
    }

    @Override
    public String toString() {
        return jsonObject.toString();
    }
}