package amadeo.phonelink;

import amadeo.phonelink.features.FeatureManager;
import amadeo.phonelink.network.manager.ConnectionManager;
import amadeo.phonelink.ui.TrayManager;
import amadeo.phonelink.ui.WindowManagerTest;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;

import javax.swing.*;
import java.util.Set;

public class PhoneLink extends Application {
    private static final String TAG = "MainApplication";
    private static final String VERSION = "0.5 ALPHA";

    @Override
    public void start(Stage primaryStage) {
        Log.setLogEnabled();
        //Log.setFilter("FeatureManager");

        Log.i(TAG, "PhoneLink version: %s", VERSION);

        Platform.setImplicitExit(false);

        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (Exception e) {
            Log.i(TAG, "Unable to load Windows look and feel");
        }

        SecurityUtils.init();
        PreferenceManager.debugMode();
        ConnectionManager.start();
        TrayManager.setupTray();
        FeatureManager.init();


        //WindowManager.showConversation("+48694724548");
        //WindowManager.showPairingWindow();
        //WindowManager.showSettingsWindow();
        //WindowManager.showNotification(new SMS("1","123","blat","cyka"));
        //WindowManager.showStageWith("stage1", new ExtendedConversationHBox(DataProvider.getConversation("123")),"Some WindowManagerTest");
        //WindowManager.showLinkNotification("1233456", "http://www.google.com/");


//        new Thread(() -> {
//            try {
//                Thread.sleep(1000);
//
//                NetPackage netPackage = new NetPackage("sms_conversation_sync_all");
//                //NetPackage netPackage = new NetPackage("blah");
//                ConnectionManager.getConnectionServer().sendNetPackage(netPackage);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }).start();


//        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
//            Log.i(TAG, "Shutdown hook");
//            PreferenceManager.savePreferences();
//            DataProvider.saveConversations();
//            ConnectionManager.cleanup();
//            FeatureManager.cleanup();
//        }));
    }

    @Override
    public void stop() {
        Log.i(TAG, "stop()");
        WindowManagerTest.getInstance().cleanup();
        PreferenceManager.savePreferences();
        DataProvider.saveConversations();
        ConnectionManager.cleanup();
        FeatureManager.cleanup();

        Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
        Thread[] threads = threadSet.toArray(new Thread[threadSet.size()]);
        System.out.println("\nThreads:\n");
        for (Thread thread : threads) {
            System.out.printf("ID: %d, Name: %s, Alive: %b, ClassName: %s\n", thread.getId(), thread.getName(), thread.isAlive(), thread.getClass().getName());
        }
    }
}
