package amadeo.phonelink;

import amadeo.phonelink.device.Phone;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.json.JSONObject;

import javax.net.ssl.*;
import java.io.IOException;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class SecurityUtils {
    private static final String TAG = "SecurityUtils";

    private static Random random = null;
    private static final String GEN_CHARACTERS = "a1bZc2dYe3fXg4hWi5jVk6lUm7nTo8pSr9sRt9uQw8yPz7qO6N5M4L3K2J1I0H1G0F2E0D3C0B4A";

    private static final BouncyCastleProvider bouncyCastleProvider = new BouncyCastleProvider();
    private static X509Certificate deviceCertificate;

    public static synchronized String generateRandomString(int min, int max) {
        if (random == null)
            random = new Random(System.currentTimeMillis());

        int stringLength = random.nextInt((max - min) + 1) + min;
        Log.i(TAG, "Generating string with %d chars", stringLength);

        StringBuilder password = new StringBuilder();

        for (int i = 0; i < stringLength; i++)
            password.append(GEN_CHARACTERS.charAt(random.nextInt(GEN_CHARACTERS.length() - 1)));

        return password.toString();
    }

    public static void init() {
        Security.addProvider(bouncyCastleProvider);

        try {
            JSONObject preferences = PreferenceManager.getPreferences("security");

            if (!preferences.has("publicKey") || !preferences.has("privateKey")) {
                Log.i(TAG, "generating device key pair");

                KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
                keyPairGenerator.initialize(2048);
                KeyPair keyPair = keyPairGenerator.generateKeyPair();

                String publicKey = Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded());
                String privateKey = Base64.getEncoder().encodeToString(keyPair.getPrivate().getEncoded());

                preferences.put("publicKey", publicKey);
                preferences.put("privateKey", privateKey);
            }

            PublicKey publicKey = getPublicKey();
            PrivateKey privateKey = getPrivateKey();

            if (!preferences.has("certificate")) {
                deviceCertificate = generateCertificate(PhoneLinkUtils.getDeviceName(), publicKey, privateKey, "SHA256withRSA");
            } else {
                byte[] deviceEncodedCertificate = Base64.getDecoder().decode(preferences.getString("certificate"));
                X509CertificateHolder certificateHolder = new X509CertificateHolder(deviceEncodedCertificate);

                deviceCertificate = new JcaX509CertificateConverter().setProvider(bouncyCastleProvider).getCertificate(certificateHolder);
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static PublicKey getPublicKey() throws InvalidKeySpecException, NoSuchAlgorithmException {
        try {
            JSONObject preferences = PreferenceManager.getPreferences("security");

            byte[] publicEncodedKey = Base64.getDecoder().decode(preferences.getString("publicKey"));

            return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(publicEncodedKey));
        } catch (Exception e) {
            throw e;
        }
    }

    public static PrivateKey getPrivateKey() throws InvalidKeySpecException, NoSuchAlgorithmException {
        try {
            JSONObject preferences = PreferenceManager.getPreferences("security");

            byte[] privateEncodedKey = Base64.getDecoder().decode(preferences.getString("privateKey"));

            return KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(privateEncodedKey));
        } catch (Exception e) {
            throw e;
        }
    }

    private static X509Certificate generateCertificate(String cn, PublicKey publicKey, PrivateKey privateKey, String sigAlgID) {
        try {
            X500NameBuilder nameBuilder = new X500NameBuilder(BCStyle.INSTANCE);
            nameBuilder.addRDN(BCStyle.CN, cn);
            nameBuilder.addRDN(BCStyle.OU, "Phone Link");
            nameBuilder.addRDN(BCStyle.O, "Amadeo");

            Calendar calendar = Calendar.getInstance();
            Date from = calendar.getTime();
            calendar.add(Calendar.YEAR, 5);
            Date to = calendar.getTime();

            X509v3CertificateBuilder certificateBuilder = new JcaX509v3CertificateBuilder(nameBuilder.build(), BigInteger.ONE, from, to, nameBuilder.build(), publicKey);

            ContentSigner contentSigner = new JcaContentSignerBuilder(sigAlgID).setProvider("BC").build(privateKey);
            X509Certificate certificate = new JcaX509CertificateConverter().setProvider("BC").getCertificate(certificateBuilder.build(contentSigner));

            return certificate;
        } catch (OperatorCreationException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static SSLContext getSSLContext(Phone phone) {
        try {
            PrivateKey privateKey = getPrivateKey();

            char[] keyStorePass = "".toCharArray();

            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(null, null);
            keyStore.setKeyEntry("privateKey", privateKey, keyStorePass, new Certificate[]{deviceCertificate});

            TrustManager[] trustManagers;
            if (phone != null && !phone.getDeviceID().equals("debugID")) {
                X509CertificateHolder certificateHolder = new X509CertificateHolder(phone.getEncodedCertificate());
                X509Certificate remoteDeviceCertificate = new JcaX509CertificateConverter().setProvider(new BouncyCastleProvider()).getCertificate(certificateHolder);
                keyStore.setCertificateEntry(phone.getDeviceID(), remoteDeviceCertificate);

                TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                trustManagerFactory.init(keyStore);
                trustManagers = trustManagerFactory.getTrustManagers();
            } else {
                Log.i(TAG, "insecure SSLContext generated!");
                trustManagers = new TrustManager[]{new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(X509Certificate[] x509Certificates, String s) {

                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] x509Certificates, String s) {

                    }

                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[0];
                    }
                }
                };
            }

            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            keyManagerFactory.init(keyStore, "".toCharArray());

            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(keyManagerFactory.getKeyManagers(), trustManagers, new SecureRandom());

            return sslContext;
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static X509Certificate getDeviceCertificate() {
        return deviceCertificate;
    }
}
